﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawGeometricShapes
{
    class Program
    {
        static void Main(string[] args)
        {
            Line line = new Line
            {
                Lenght = 15,
                Symbol = '*',
                RndColor = true
            };
            line.Draw();
            Console.WriteLine();
            Rectangle rectangle = new Rectangle();
            rectangle.Draw(10, 5, '*', true);
            Console.WriteLine();
            Square square = new Square();
            square.Draw(5, '*', true);
            Console.WriteLine();
            Triangle triangle = new Triangle();
            triangle.Draw(5, '*', true);
            Console.WriteLine();
            Arrow arrow = new Arrow
            {
                Lenght = 20,
                Symbol = '*',
                RndColor = true,
                Direction = "r"
            }
            ;
            arrow.Draw();

            Console.ReadKey();
        }
    }
}
