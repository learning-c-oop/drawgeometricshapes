﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawGeometricShapes
{
    class Line
    {
        public int Lenght { get; set; }
        public char Symbol { get; set; }
        public bool RndColor { get; set; }
        public ConsoleColor Color { get; set; }
        public virtual void Draw()
        {
            Console.WriteLine("Drawing Line");
            if (RndColor == true)
            {
                Random rnd = new Random();
                for (int i = 0; i < Lenght; i++)
                {
                    Console.ForegroundColor = (ConsoleColor)rnd.Next(1, 16);
                    Console.Write(Symbol);
                }
            }
            else
            {
                Console.ForegroundColor = Color;
                for (int i = 0; i < Lenght; i++)
                {
                    Console.Write(Symbol);
                }

            }
            Console.ResetColor();
        }
    }
}
