﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawGeometricShapes
{
    class Arrow : Line
    {
        public string Direction { get; set; }
        public override void Draw()
        {
            Console.WriteLine("Drawing Arrow");
            string rightOrLeft = Direction.ToLower();
            if (RndColor == true)
            {
                Random rnd = new Random();
                switch (rightOrLeft[0])
                {
                    case 'r':
                        Console.WriteLine(new string(' ', (Lenght * 2) - 4) + Symbol);
                        for (int i = 0; i < Lenght; i++)
                        {
                            Console.ForegroundColor = (ConsoleColor)rnd.Next(1, 16);
                            Console.Write(Symbol + " ");
                        }
                        Console.WriteLine();
                        Console.WriteLine(new string(' ', (Lenght * 2) - 4) + Symbol);
                        break;
                    case 'l':
                        Console.WriteLine(new string(' ', 2) + Symbol);
                        for (int i = 0; i < Lenght; i++)
                        {
                            Console.ForegroundColor = (ConsoleColor)rnd.Next(1, 16);
                            Console.Write(Symbol + " ");
                        }
                        Console.WriteLine();
                        Console.WriteLine(new string(' ', 2) + Symbol);
                        break;
                }
            }
            else
            {
                Console.ForegroundColor = Color;
                switch (rightOrLeft[0])
                {
                    case 'r':
                        Console.WriteLine(new string(' ', (Lenght * 2) - 4) + Symbol);
                        for (int i = 0; i < Lenght; i++)
                        {
                            Console.Write(Symbol + " ");
                        }
                        Console.WriteLine();
                        Console.WriteLine(new string(' ', (Lenght * 2) - 4) + Symbol);
                        break;
                    case 'l':
                        Console.WriteLine(new string(' ', 2) + Symbol);
                        for (int i = 0; i < Lenght; i++)
                        {
                            Console.Write(Symbol + " ");
                        }
                        Console.WriteLine();
                        Console.WriteLine(new string(' ', 2) + Symbol);
                        break;
                }
            }
            Console.ResetColor();
        }
    }
}
