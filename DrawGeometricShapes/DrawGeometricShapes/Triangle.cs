﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawGeometricShapes
{
    class Triangle
    {
        public void Draw(int height, char symbol, bool rndColor, ConsoleColor color = ConsoleColor.Red)
        {
            Console.WriteLine("Drawing Triangle");
            if (rndColor == true)
            {
                Random rnd = new Random();
                for (int i = 0; i < height; i++)
                {
                    for (int j = 0; j < i + 1; j++)
                    {
                        Console.ForegroundColor = (ConsoleColor)rnd.Next(1, 16);
                        Console.Write(symbol);
                    }
                    Console.WriteLine();
                }
            }
            else
            {
                Console.ForegroundColor = color;
                for (int i = 0; i < height; i++)
                {
                    for (int j = 0; j < i + 1; j++)
                    {
                        Console.Write(symbol);
                    }
                }
            }
            Console.ResetColor();
        }
    }
}
