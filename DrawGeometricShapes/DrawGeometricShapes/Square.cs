﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawGeometricShapes
{
    class Square
    {
        public void Draw(int lenght, char symbol, bool rndColor, ConsoleColor color = ConsoleColor.Red)
        {
            Console.WriteLine("Drawing Square");
            if (rndColor == true)
            {
                Random rnd = new Random();
                for (int i = 0; i < lenght; i++)
                {
                    for (int j = 0; j < lenght; j++)
                    {
                        Console.ForegroundColor = (ConsoleColor)rnd.Next(1, 16);
                        Console.Write(symbol + " ");
                    }
                    Console.WriteLine();
                }
            }
            else
            {
                Console.ForegroundColor = color;
                for (int i = 0; i < lenght; i++)
                {
                    for (int j = 0; j < lenght; j++)
                    {
                        Console.Write(symbol + " ");
                    }
                    Console.WriteLine();
                }
            }
            Console.ResetColor();
        }
    }
}
